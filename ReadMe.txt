Library for protocol parsing S.Bus. The efficiency was tested with Radiolink AT10II transmitter, R12DS receiver and Arduino UNO board.
Connection must be made with using some inverter circuit. It is recommended to use an logical inverter chip.
I use the simplest inverter on one NPN transistor (the drawing is attached).