
#include "DgelioncelSBus.h"

void setup() {
  openSerialForSBus();
}

void loop() {

  recognizeSBus();

  if (failSafe)
  {
    Serial.println("FailsafeMode");
  }
  else if (noConnection)
  {
    Serial.println("NoConnection");
  }
  else
  {
    for (int i = 0; i < 12; ++i)
    {
      Serial.print(sbusCh[i]);
      Serial.print(" ");
    }
    Serial.println();
  }

}
