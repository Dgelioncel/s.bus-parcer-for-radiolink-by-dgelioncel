#ifndef __DGELIONCEL_SBUS__
#define __DGELIONCEL_SBUS__

#include "Arduino.h"

extern bool failSafe;
extern bool pocketLost;
extern bool noConnection;
extern bool manualErrorByTime;

extern uint16_t sbusCh[12];

uint8_t partFromByte(uint8_t value, uint8_t mask, uint8_t amountOfUselessZeros = 0);

void openSerialForSBus();

void recognizeSBus();

#endif