
#include "DgelioncelSBus.h"

// Constants:
#define SBUS_HEADER 0x0F
#define SBUS_END 0x00
#define SBUS_SIZE 25
#define NO_FRAME_ERROR_TIME 500

// Flags:
bool failSafe = false;
bool pocketLost = true;
bool noConnection = true;
bool manualErrorByTime = true;

// Manual error:
unsigned long lastFrameRefresh = millis();

// S.Bus pocket src:
uint8_t sbusArray[SBUS_SIZE];
// S.Bus channels:
uint16_t sbusCh[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

uint8_t partFromByte(uint8_t value, uint8_t mask, uint8_t amountOfUselessZeros = 0)
{
	return (value & mask) >> amountOfUselessZeros;
}

void openSerialForSBus()
{
	Serial.begin(100000, SERIAL_8E2);
}

void recognizeSBus()
{
	if (millis() - lastFrameRefresh > NO_FRAME_ERROR_TIME)
		manualErrorByTime = true;
	else
		manualErrorByTime = false;
	if (Serial.available())
	{
		uint8_t currentByte = Serial.read();
		uint8_t pocketSize = 0;
		if (currentByte == SBUS_HEADER)
		{
			sbusArray[pocketSize] = currentByte;
			pocketSize++;
			unsigned long whileTime = millis();
			while (pocketSize < SBUS_SIZE)
			{
				if (Serial.available())
				{
					sbusArray[pocketSize] = Serial.read();
					pocketSize++;
				}
				if (millis() - whileTime > 50)
				{
					break;
				}
			}
			if (sbusArray[SBUS_SIZE - 1] == SBUS_END)
			{
				//Serial.write(sbusArray[i]);
				sbusCh[0] = uint16_t(sbusArray[1]) | (uint16_t(partFromByte(sbusArray[2], 0b00000111)) << 8);
				sbusCh[1] = (uint16_t(partFromByte(sbusArray[2], 0b11111000, 3))) | (uint16_t(partFromByte(sbusArray[3], 0b00111111)) << 5);
				sbusCh[2] = (uint16_t(partFromByte(sbusArray[5], 0b00000001)) << 10) | (uint16_t(sbusArray[4]) << 2) | (uint16_t(partFromByte(sbusArray[3], 0b11000000), 6));
				sbusCh[3] = (uint16_t(partFromByte(sbusArray[5], 0b11111110), 1)) | (uint16_t(partFromByte(sbusArray[6], 0b00001111)) << 7);
				sbusCh[4] = (uint16_t(partFromByte(sbusArray[6], 0b11110000), 4)) | (uint16_t(partFromByte(sbusArray[7], 0b01111111)) << 4);
				sbusCh[5] = (uint16_t(partFromByte(sbusArray[9], 0b00000011)) << 9) | (uint16_t(sbusArray[8]) << 1) | (uint16_t(partFromByte(sbusArray[7], 0b10000000), 7));
				sbusCh[6] = (uint16_t(partFromByte(sbusArray[9], 0b11111100), 2)) | (uint16_t(partFromByte(sbusArray[10], 0b00011111)) << 6);
				sbusCh[7] = (uint16_t(partFromByte(sbusArray[10], 0b11100000), 5)) | (uint16_t(sbusArray[11]) << 3);
				sbusCh[8] = uint16_t(sbusArray[12]) | (uint16_t(partFromByte(sbusArray[13], 0b00000111)) << 8);
				sbusCh[9] = (uint16_t(partFromByte(sbusArray[13], 0b11111000, 3))) | (uint16_t(partFromByte(sbusArray[14], 0b00111111)) << 5);
				sbusCh[10] = (uint16_t(partFromByte(sbusArray[16], 0b00000001)) << 10) | (uint16_t(sbusArray[15]) << 2) | (uint16_t(partFromByte(sbusArray[14], 0b11000000), 6));
				sbusCh[11] = (uint16_t(partFromByte(sbusArray[16], 0b11111110), 1)) | (uint16_t(partFromByte(sbusArray[17], 0b00001111)) << 7);

				if (partFromByte(sbusArray[23], 0b00000100, 2))
					failSafe = true;
				else
					failSafe = false;
				if (partFromByte(sbusArray[23], 0b0001000, 3))
					pocketLost = true;
				else
					pocketLost = false;
				lastFrameRefresh = millis();
			}
		}
	}

	if (failSafe || pocketLost || manualErrorByTime)
		noConnection = true;
	else
		noConnection = false;
}